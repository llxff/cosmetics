<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 28.11.12
 * Time: 17:46
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Controller;
use Controller\Abstraction\Controller;
use Application\Domain\Cosmetics\Brands;
use View\Infrastructure\Result\JsonResult;
use Application\Domain\Cosmetics\Cosmetics;

class HomeController extends Controller
{
    const DEPLOY = 0;
    const RANDOM = 1;
    const DEBUG = 2;


    public function GetExpirationDate($code, $brandnum, $test = self::DEPLOY)
    {
        $Cosmetics = new Cosmetics($code, $brandnum, $this->isRandomMode($test));

        $Cosmetics->check();

        $data = array();

        if($Cosmetics->isValid())
        {
            if($Cosmetics->isDateOfManufacturerDefined())
            {
                $data['dateOfManufacturer'] = $Cosmetics->getDateOfManufacturer()->getTimestamp();
            }
            else
            {
                $data['yearOfManufacturer'] = $Cosmetics->getYearOfManufacturer();
            }

            $data += array(
                'shelfLife' => $Cosmetics->getShelfLife(),
                'validUntil' => $Cosmetics->getValidUntil(),
                'outdated' => $Cosmetics->isOutdated()
            );
        }
        else
        {
            $data = array('error' => 'specified code is not correct, or not supported');
        }

        if($this->isDebugMode($test))
        {
            $data['rawResponse'] = $Cosmetics->getRawResponse();
        }

        return new JsonResult($data);
    }

    private function isRandomMode($mode)
    {
        return bindec($mode) & self::RANDOM;
    }

    private function isDebugMode($mode)
    {
        return bindec($mode) & self::DEBUG;
    }

    public function GetBrands()
    {
        $Brands = new Brands();

        return new JsonResult($Brands->getBrands());
    }
}
