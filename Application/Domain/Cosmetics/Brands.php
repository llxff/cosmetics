<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 28.11.12
 * Time: 18:55
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Domain\Cosmetics;

class Brands
{
    const URL = 'http://checkcosmetic.net/';

    public function getBrands()
    {
        return $this->parseBrands($this->loadRawBrands());
    }

    private function loadRawBrands()
    {
        $ch = curl_init(self::URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        return curl_exec($ch);
    }

    private function parseBrands($response)
    {
        preg_match("`<select name='brandid' tabindex='1' id='brandid'>(.*?)</select>`s", $response, $matches);

        if(isset($matches[1]))
        {
            preg_match_all("`<option value='(\\d+)'>(.*?)</option>`", $matches[1], $brands);

            $brandsGroup = array();

            if(sizeof($brands) == 3)
            {
                $ids = $brands[1];
                $names = $brands[2];

                foreach($ids as $key => $id)
                {
                    $brandsGroup[] = array('id' => $id, 'brand' => $names[$key]);
                }
            }

           return $brandsGroup;
        }

        return array();
    }
}
