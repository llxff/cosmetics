<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 04.12.12
 * Time: 18:18
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Domain\Cosmetics;

class CachedCosmetics
{

    private $sig;
    private $object;
    private $checked = false;

    function __construct($code, $brandnum, $testMode = false)
    {
        $this->sig = md5("$code:$brandnum:$testMode");

        $this->object = new Cosmetics($code, $brandnum, $testMode);
    }

    public function __call($name, $args)
    {
        $key = "$this->sig:$name";

        if($name !== 'check')
        {
            $value = apc_fetch($key, $success);
            var_dump($key);

            if($success) {
                return $value;
            }
            else
            {
                if(!$this->checked)
                {
                    $this->object->check();
                    $this->checked = true;
                }

                $result = call_user_func([$this->object, $name]);
                var_dump(apc_store($key, $result, 2 * 60));

                return $result;
            }
        }

        return false;
    }
}
