<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 28.11.12
 * Time: 17:55
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Domain\Cosmetics;

use DateTime;
use Application\Domain\Cosmetics\DataExtractors\ValidUntilExtractor;
use Application\Domain\Cosmetics\DataExtractors\ShelfLifeExtractor;
use Application\Domain\Cosmetics\DataExtractors\DateOfManufacturerExtractor;

class Cosmetics
{
    private $code;
    private $brandNum;

    const URL = 'http://checkcosmetic.net/wp-admin/admin-ajax.php';
    const ACITON = 'dc_checkAJAX';

    private $isValid = false;
    private $dateOfManufacturer = null;
    private $shelfLife = null;
    private $validUntil = null;
    private $outdated = false;
    private $yearOfManufacturer = null;

    private $testMode = false;

    private $rawResponse;

    function __construct($code, $brandnum, $testMode = false)
    {
        $this->code = $code;
        $this->brandNum = $brandnum;

        $this->testMode = $testMode;
    }

    public function check()
    {
        $ch = curl_init(self::URL);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Requested-With: XMLHttpRequest'));

        if(!$this->testMode)
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Origin:http://checkcosmetic.net'));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Referer:http://checkcosmetic.net/'));
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('action' => self::ACITON, 'code' => $this->code, 'brandnum' => $this->brandNum));

        $this->parseResponse(curl_exec($ch));
    }

    private function parseResponse($response)
    {
        $this->rawResponse = $response;

        if($this->isFreshProduct($response))
        {
            $this->shelfLife = $this->extractShelfLife($response);
            $this->validUntil = $this->extractValidUntil($response);
            $this->dateOfManufacturer = $this->extractDate($response);

            $this->isValid = true;
            $this->outdated = false;
        }
        else if($this->isOutdatedProduct($response))
        {
            $this->shelfLife = $this->extractShelfLife($response);
            $this->dateOfManufacturer = $this->extractDate($response);

            $this->isValid = true;
            $this->outdated = true;
        }
        else
        {
            $this->isValid = false;
        }
    }

    private function isFreshProduct($response)
    {
        return preg_match('`<span style="font-weight:bold;">.*?</span>.*General shelf life: \d+ months.* \d+ (months|years?|days)</span>`', $response);
    }

    private function isOutdatedProduct($response)
    {
        return preg_match('`<span style="font-weight:bold;">.*?</span>.*General shelf life: \d+ months.*It looks like your product was manufactured`', $response);
    }

    private function extractDate($date)
    {
        $DateExtractor = new DateOfManufacturerExtractor($date);

        if($DateExtractor->isFoundedAnything())
        {
            if($DateExtractor->isHaveYearOnly())
            {
                $this->yearOfManufacturer = $DateExtractor->getYear();
            }
            else if($DateExtractor->isHaveFullDate())
            {
                return $DateExtractor->getDate();
            }
        }

        return null;
    }

    private function extractShelfLife($rawString)
    {
        $Extractor = new ShelfLifeExtractor($rawString);

        return $Extractor->getShelfLife();
    }

    private function extractValidUntil($rawString)
    {
        $Extractor = new ValidUntilExtractor($rawString);

        return $Extractor->getValidUntil();
    }

    /**
     * @return \DateTime
     */
    public function getDateOfManufacturer()
    {
        return $this->dateOfManufacturer;
    }

    public function isValid()
    {
        return $this->isValid;
    }

    public function isOutdated()
    {
        return $this->outdated;
    }

    public function getShelfLife()
    {
        return $this->shelfLife;
    }

    public function getValidUntil()
    {
        return $this->validUntil;
    }

    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    public function getYearOfManufacturer()
    {
        return $this->yearOfManufacturer;
    }

    public function isDateOfManufacturerDefined()
    {
        return is_object($this->getDateOfManufacturer());
    }
}
