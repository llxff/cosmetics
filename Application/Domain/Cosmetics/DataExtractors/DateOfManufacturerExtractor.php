<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 05.12.12
 * Time: 12:14
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Domain\Cosmetics\DataExtractors;
use DateTime;

class DateOfManufacturerExtractor
{
    private $foundedAnything = false;

    private $baseString = '';

    private $haveFullDate = false;
    private $fullDate;

    private $haveYearOnly = false;
    private $year;

    function __construct($string)
    {
        $this->baseString = $this->foundDateStingIn($string);

        if($this->baseString !== null)
        {
            if($this->isDateStringHaveYearOnly())
            {
                $this->haveYearOnly = true;
                $this->foundedAnything = true;

                $this->year = $this->baseString;
            }
            else
            {
                $this->appendDayIfNotExists();

                $date = DateTime::createFromFormat('d of F Y H i', $this->baseString.' 00 00');

                if($date === null)
                {
                    $this->foundedAnything = false;
                }
                else
                {
                    $this->foundedAnything = true;
                    $this->haveFullDate = true;

                    $this->fullDate = $date;
                }
            }
        }
    }

    private function foundDateStingIn($string)
    {
        if(preg_match('`<span style="font-weight:bold;">(.*?)</span>`', $string, $date))
        {
            if(isset($date[1]))
            {
                return $this->clearStringFromComma($date[1]);
            }
        }

        return null;
    }

    private function clearStringFromComma($string)
    {
        return str_replace(',', '', $string);
    }

    private function appendDayIfNotExists()
    {
        if(preg_match('`\d+ of`', $this->baseString))
        {
            return ;
        }

        $this->baseString = '01 of '.$this->baseString;
    }


    private function isDateStringHaveYearOnly()
    {
        return preg_match('`^\d+$`', $this->baseString);
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->fullDate;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function isHaveYearOnly()
    {
        return $this->haveYearOnly;
    }

    public function isHaveFullDate()
    {
        return $this->haveFullDate;
    }

    public function isFoundedAnything()
    {
        return $this->foundedAnything;
    }
}
