<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 05.12.12
 * Time: 12:15
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Domain\Cosmetics\DataExtractors;

class ShelfLifeExtractor
{
    private $rawString;

    private $shelfLife;

    function __construct($rawString)
    {
        $this->rawString = $rawString;

        $this->shelfLife = $this->extract();
    }

    private function extract()
    {
        preg_match('`General shelf life: (\d+) months`', $this->rawString, $matches);

        if(isset($matches[1]))
        {
            return $matches[1];
        }

        return null;
    }

    public function isValid()
    {
        return $this->shelfLife !== null;
    }

    public function getShelfLife()
    {
        return $this->shelfLife;
    }
}
