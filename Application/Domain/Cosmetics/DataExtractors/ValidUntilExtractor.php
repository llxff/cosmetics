<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 05.12.12
 * Time: 12:17
 * To change this template use File | Settings | File Templates.
 */
namespace Application\Domain\Cosmetics\DataExtractors;

class ValidUntilExtractor
{
    private $rawString;

    private $validUntil;

    function __construct($rawString)
    {
        $this->rawString = $rawString;

        $this->validUntil = $this->extract();
    }

    private function extract()
    {
        preg_match('`(\d+) (months|years?|days)</span>`', $this->rawString, $matches);

        if(isset($matches[1], $matches[2]))
        {
            switch($matches[2])
            {
                case 'months': return $matches[1];
                case 'years' || 'year' : return 12 * $matches[1];
            }
        }

        return null;
    }

    public function isValid()
    {
        return $this->validUntil !== null;
    }

    public function getValidUntil()
    {
        return $this->validUntil;
    }
}
