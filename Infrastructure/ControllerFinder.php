<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 07.11.12
 * Time: 1:31
 * To change this template use File | Settings | File Templates.
 */
namespace Infrastructure;

class ControllerFinder
{
    private $ControllerClass;

    public function find($Controller)
    {
        $this->ControllerClass = $this->findController($Controller);
    }

    protected function findController($ControllerName)
    {
        $ControllerPaths = $this->registerControllersPaths();


        foreach($ControllerPaths as $ControllerPath)
        {
            $Controller = $ControllerPath.$ControllerName.'Controller';

            if(class_exists($Controller))
            {
                return $Controller;
            }
        }

        return null;
    }

    protected function registerControllersPaths()
    {
        $ControllerPath[] = '\\Application\\Controller\\';

        return $ControllerPath;
    }

    public function isControllerFounded()
    {
        return $this->ControllerClass !== null;
    }

    public function getController()
    {
        return $this->ControllerClass;
    }
}
