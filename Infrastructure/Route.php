<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 06.11.12
 * Time: 22:00
 * To change this template use File | Settings | File Templates.
 */
namespace Infrastructure;

class Route extends SimpleRoute
{
    private $route;

    function __construct($route)
    {
        $this->route = $route;
    }

    public function define()
    {
	$route = explode('?', $this->route);
        $routeSettings = explode('/', $route[0]);

        if(isset($routeSettings[1]) && trim($routeSettings[1]) != '')
        {
            $this->Controller = trim($routeSettings[1]);
        }
        else if ($this->Controller === null)
        {
            $this->Action = null;
            return ;
        }

        if(isset($routeSettings[2]) && trim($routeSettings[2]) != '')
        {
            $this->Action = trim($routeSettings[2]);
        }
    }
}
