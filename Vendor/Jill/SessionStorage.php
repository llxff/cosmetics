<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 13.11.12
 * Time: 0:18
 * To change this template use File | Settings | File Templates.
 */
namespace Vendor\Jill;

use ArrayAccess;

class SessionStorage implements ArrayAccess
{
    const LIFETIME = '___jill_session_lifetime';
    const BORN = '___jill_session_born';
    const FLASH = '___jill_flash';

    private static $instance;

    public static function storage()
    {
        if(self::$instance === null)
        {
            return self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
        session_start();
    }

    public function offsetExists($offset)
    {
        $dataExists = isset($_SESSION[$offset]);

        if($dataExists)
        {
            if($this->isTemporaryDataExpired($_SESSION[$offset]))
            {
                    $this->offsetUnset($offset);
                    return false;
            }
        }

        return $dataExists;
    }

    private function isTemporaryData($value)
    {
        return $this->hasLifetime($value) && isset($value[self::BORN]);
    }

    private function isTemporaryDataExpired($value)
    {
        return $this->isTemporaryData($value) && !($value[self::BORN] + $value[self::LIFETIME] >= time());
    }

    private function hasLifetime($value)
    {
        return isset($value[self::LIFETIME]);
    }

    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $_SESSION[$offset] : null;
    }

    public function offsetSet($offset, $value)
    {
        if($this->hasLifetime($value))
        {
            $value += array(self::BORN => time());
        }

        $_SESSION[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($_SESSION[$offset]);
    }

    public function setTemp($offset, $value, $expiredTimeInSec)
    {
        $this->offsetSet($offset, array('value' => $value) + array(self::LIFETIME => (int) $expiredTimeInSec));
    }

    public function getTemp($offset)
    {
        $temp = $this->offsetGet($offset);

        if(isset($temp['value']))
        {
            return $temp['value'];
        }

        return null;
    }

    public function setFlash($offset, $value)
    {
        $_SESSION[$offset] = $value + array(self::FLASH => true);
    }

    public function getFlash($offset)
    {
        if($this->offsetExists($offset) && isset($_SESSION[self::FLASH]))
        {
            $value = $_SESSION[$offset];
            $this->offsetUnset($offset);

            return $value;
        }

        return null;
    }
}
