<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 07.11.12
 * Time: 2:03
 * To change this template use File | Settings | File Templates.
 */
namespace View\Infrastructure\Concrete;
use View\Infrastructure\Abstraction\View;

class DefaultView extends View
{
    protected function findTemplate($Controller, $Action, $Views)
    {
        foreach($Views as $view)
        {
            $view = $view.$Controller.'/'.$Action.'.html';

            if(file_exists(ROOTDIR.$view))
            {
                return $view;
            }
        }

        return null;
    }

    protected function registerViewsPath()
    {
        $Views[] = '/Application/View/';
        $Views[] = '/View/Templates/';

        return $Views;
    }

    protected function renderTemplate($view, $data)
    {
        return $this->templateEngine()->render($view, $data);
    }

    private function templateEngine()
    {
        static $engine;

        if($engine === null)
        {
            require_once ROOTDIR . '/Vendor/twig/lib/Twig/Autoloader.php';

            \Twig_Autoloader::register();

            $mainloader = new \Twig_Loader_Filesystem(ROOTDIR);

            $twig_params = array('optimizations' => 1);

            $engine = new \Twig_Environment($mainloader, $twig_params);

            return $engine;
        }

        return $engine;

    }
}
