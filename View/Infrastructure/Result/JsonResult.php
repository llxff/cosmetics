<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 28.11.12
 * Time: 18:50
 * To change this template use File | Settings | File Templates.
 */
namespace View\Infrastructure\Result;

class JsonResult
{
    private $result;

    function __construct(array $result)
    {
        $this->result = json_encode($result);
    }

    public function __toString()
    {
        header('Content-Type: application/json');
        return $this->result;
    }
}
