<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ll
 * Date: 06.11.12
 * Time: 21:12
 * To change this template use File | Settings | File Templates.
 */
require '../ClassLoader.php';
ClassLoader::load();

use Infrastructure\Route;

$Route = new \Infrastructure\Route(str_replace('/cosmetics', '', $_SERVER['REQUEST_URI']));
$Route->Controller = 'Home';
$Route->Action = 'Index';

echo Application::instance()->run($Route);